.\" Copyright (c) 2018  Peter Pentchev
.\" All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
.\" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.Dd April 21, 2018
.Dt FEATURE-CHECK 1
.Os
.Sh NAME
.Nm feature-check
.Nd query a program for supported features
.Sh SYNOPSIS
.Nm
.Op Fl v
.Op Fl O Ar optname
.Op Fl P Ar prefix
.Ar program
.Ar feature-name
.Nm
.Op Fl O Ar optname
.Op Fl P Ar prefix
.Ar program
.Ar feature-name
.Ar op
.Ar version
.Nm
.Op Fl O Ar optname
.Op Fl o Ar json|tsv
.Op Fl P Ar prefix
.Fl l
.Ar program
.Nm
.Fl V | Fl h
.Nm
.Fl -features
.Sh DESCRIPTION
The
.Nm
tool obtains the list of supported features from a program via various
methods (e.g. running it with the
.Fl -features
command-line option) and allows other programs to check for the presence
and, possibly, versions of specific features.
.Pp
Options:
.Bl -tag -width indent
.It Fl -features
List the compile-time features supported by this version of
.Nm :
.Bl -tag -width indent
.It Dv feature-check
Always present, with the version of the
.Nm
tool.
.It Dv list
Present if the
.Nm
tool supports fetching the list of features supported by a program.
.It Dv simple
Present if the
.Nm
tool supports comparing the version of a single feature against
a specified version string.
.It Dv single
Present if the
.Nm
tool supports querying a single feature.
.El
.It Fl h , Fl -help
Display program usage output and exit.
.It Fl O Ar optname
Specify the option to pass to the program to obtain its list of
features (default
.Fl -features ) .
.It Fl o Ar format
Specify the output format for the full list of features with the
.Fl l
option.
.It Fl P Ar prefix
Specify the prefix that
.Nm
should look for in the program's output to find the line that contains
the list of features; default "Features: ".
.It Fl V , Fl -version
Display program version output and exit.
.It Fl v
When querying a single feature, output the version obtained for it.
.El
.Sh EXIT STATUS
When querying a single feature,
.Nm
will exit with code 0 if the program supports that feature, and with
a non-zero code when either the program could not be executed, or
its list of features could not be parsed, or the feature was not present.
.Pp
When examining the version of a single feature,
.Nm
will exit with code 0 if the comparison result is true, and with
a non-zero code when either the program could not be executed, or
its list of features could not be parsed, or the comparison failed.
.Pp
When listing the features supported by a program,
.Nm
will exit with code 0 when the list of features was obtained and output,
and with a non-zero code when either the program could not be executed or
its list of features could not be parsed.
.Sh EXAMPLES
.Pp
The following examples are shown as given to the shell:
.Pp
.Dl feature-check curl AsynchDNS
.Pp
Exit with code 0 if the
.Xr curl 1
tool is present and it supports asynchronous DNS lookups.
.Pp
.Dl feature-check -l confget
.Pp
List the features supported by the
.Xr confget 1
tool.
.Pp
.Dl feature-check timelimit 'timelimit ge 1.9'
.Pp
Exit with code 0 if the
.Xr timelimit 1
tool advertises its
.Dv timelimit
feature at version 1.9 or later.
.Pp
.Dl feature-check timelimit timelimit ge 1.9
.Pp
The same check; note that the
.Nm
tool will treat all the arguments after the program name as a single
expression, i.e. it will concatenate them into a single string separated
by spaces and then interpret the string.
.Sh STANDARDS
No standards documentation was harmed in the process of creating
.Nm .
.Sh BUGS
Please report any bugs in
.Nm
to the author.
.Sh AUTHOR
The
.Nm
utility was written by
.An Peter Pentchev Aq roam@ringlet.net
in 2018.
