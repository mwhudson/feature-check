#!/bin/sh

set -e

# Find the available Python 3.x versions.

interpreters=''
if [ -n "$(command -v py3versions 2>/dev/null)" ]; then
	for ver in $(py3versions -s -v); do
		interpreters="$interpreters python$ver"
	done
fi

# Finally run the tests.

test_script='test-python/python/python_fcheck.sh'
mkdir -p -- "$(dirname -- "$test_script")"

for python in $interpreters; do
	printf -- '\n\n============ Testing %s\n\n' "$python"
	echo "'$python'"' -m feature_check "$@"' > "$test_script"
	cat -- "$test_script"
	chmod +x -- "$test_script"
	env TEST_PROG="$test_script" prove t
done

printf -- '\n\n============ The TAP tests passed for all Python versions\n\n'
