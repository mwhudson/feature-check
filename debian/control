Source: feature-check
Section: utils
Priority: optional
Maintainer: Peter Pentchev <roam@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 dh-sequence-python3,
 libjson-xs-perl <!nocheck>,
 libtest-command-perl <!nocheck>,
 perl <!nocheck>,
 python3-all,
 python3-ddt <!nocheck>,
 python3-pytest <!nocheck>,
 python3-six <!nocheck>,
 python3-setuptools
Standards-Version: 4.5.0
Vcs-Git: https://salsa.debian.org/roam/feature-check.git
Vcs-Browser: https://salsa.debian.org/roam/feature-check/
Homepage: https://devel.ringlet.net/misc/feature-check/
Rules-Requires-Root: no

Package: feature-check
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${perl:Depends},
 libjson-xs-perl
Description: tool to query a program for supported features
 The feature-check tool obtains the list of supported features from
 a program via various methods (e.g. running it with the --features
 command-line option) and allows other programs to check for
 the presence and, possibly, versions of specific features.
 .
 This package contains the command-line tool for use by any program.

Package: python3-feature-check
Section: python
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${python3:Depends}
Recommends:
 ${python3:Recommends}
Suggests:
 ${python3:Suggests}
Provides:
 ${python3:Provides}
Description: query a program for supported features - Python 3.x library
 The feature-check tool obtains the list of supported features from
 a program via various methods (e.g. running it with the --features
 command-line option) and allows other programs to check for
 the presence and, possibly, versions of specific features.
 .
 This package contains the Python 3.x library.
