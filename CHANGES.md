Change log for feature-check
============================

0.2.2 - 2019/01/09
------------------
- Fix the Python command-line tool's use of the standard Python
  JSON module if simplejson is not installed.
- Remove two unnecessary "pass" statements since the functions
  have a doc-string that serves as a null body.

0.2.1 - 2018/11/23
------------------

- Build universal Python wheels.
- Do not require the Python typing module, only use it when it is
  available, e.g. during type checking tests.

0.2.0 - 2018/11/22
------------------

- Reorganize the Python implementation:
  - break it into modules
  - add flake8 and pylint checks
  - add some simple unit tests
  - add type hints

0.1.1 - 2018/05/08
------------------

- Perl 5.10 does not understand the ellipsis unimplemented statement,
  so replace it with a die() statement with an internal error message

0.1.0 - 2018/04/22
------------------

- first public release

Comments: Peter Pentchev <roam@ringlet.net>
