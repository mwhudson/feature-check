#!/bin/sh

env PYTHONPATH="$(dirname -- "$(dirname -- "$0")")${PYTHONPATH+${PYTHONPATH}}" \
    python2 -m feature_check "$@"
